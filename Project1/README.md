To get this to run on UTD's servers, you need to install maven. But this is easy;
download & unzip maven while using a utd *net* machine:

  wget http://mirror.nexcess.net/apache/maven/maven-3/3.1.1/binaries/apache-maven-3.1.1-bin.zip
  unzip apache-maven-3.1.1-bin.zip
  
Then set the following variables:

  export M2_HOME=~/apache-maven-3.1.1/
  export M2=$M2_HOME/bin

Make sure the binaries can be found on path to make your life easy :) :

  export PATH=$M2:$PATH

Run make sure that mvn --version gives you the output you expect;

  mvn --version

    ###Expected output:###
    Apache Maven 3.1.1 (0728685237757ffbf44136acec0402957f723d9a; 2013-09-17 10:22:22-0500)
    Maven home: /home/005/k/ka/kam103020/apache-maven-3.1.1
    Java version: 1.7.0_25, vendor: Oracle Corporation
    Java home: /local/jdk1.7.0_25/jre
    Default locale: en_US, platform encoding: UTF-8
    OS name: "linux", version: "2.6.32-431.el6.x86_64", arch: "amd64", family: "unix"

just unzip my project into a directory on one of the net machines, then cd into the unzipped folder...

Now you should be ready to use maven to both manage dependencies and compile my project for you:
  mvn package                         # maven should download a lot of stuff
  mvn clean compile assembly:single

To invoke my project as a single instance (where NODE_ID is some value that corresponds to the config.xml): 
  java -cp ./Project1-0.0.1-SNAPSHOT-jar-with-dependencies.jar AOSAssignments.Project1.App NODE_ID

To run with any config across all nodes (distributed or local), copy a config file atop the "config.xml" 

The behavior of the application is set up as follows: for every "neighbor" described in the xml file, it sends a random number of messages with random wait times between sending for each node, to that neighbor that simply include who they are from and the piggyback'ed clock. The relationship is n-ary; it doesn't matter how many neighbors each node has, so I forced them to all talk to all other neighbors. In addition, the sequence in which each node talks to its neighbors has been set by random.

The app may appear somewhat slow; if you start up each of the runs by hand, it should eventually exit. A run may take around a minute or two on each machine; this is because of the random wait times. But it runs reliably; it doesn't matter when you start each node; each will reliably wait until the last node has been started, has received a "done" message from all other nodes. 

The simplest configuration to run is simple_config.xml. What simple_config does is create a threeway conversation that are all local to a single machine. To make it work, do:

  cp simple_config.xml config.xml   #replace the file, it's fine

Then log into the same net machine from 3 different terminals and run with NODE_ID's of 0, 1 and 2.

The next configuration follows in the likeness of the example given in the supplied config. It's call net_config. To run it:

  cp net_config.xml config.xml

Then, log into the machines listed by the config file and pass the NODE_ID's 0 to 6, corresponding exactly with the net machine's id. Run the examples manually; I didn't write a script, because I think you should see it work-there's nothing stopping you from throwing a script at it so long as you follow the format.

To test my code, you can easily write your own config files. Make sure that the nodeCount value at the top corresponds exactly to the number you place below; it assumed the file is properly set up.
