package AOSAssignments.Project1;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

public class VectorClock {
	private static VectorClock singletonClock=null;
	private HashMap<Integer, Integer> vclock; //node ID to clock
	
	private VectorClock() {
		vclock = new HashMap<Integer, Integer>(App.config.nodeCount);
	}
	
	private VectorClock(HashMap<Integer, Integer> internalClock) {
		vclock=internalClock;
	}
	
	public static VectorClock getVectorClock() {
		if (singletonClock==null) {
			singletonClock = new VectorClock();
			Iterator it = App.config.neighbors.iterator();
			while (it.hasNext()) {
				singletonClock.vclock.put(((Config.Node) it.next()).id, 0);
			}
		}
		return singletonClock;
	}
	
	public static VectorClock constructFromString(String clockString) {
		HashMap<Integer, Integer> remoteVClock = 
				new HashMap<Integer, Integer>();
		Scanner intFinder = new Scanner(clockString).useDelimiter("[^0-9]+");
		while (intFinder.hasNext()) {
			remoteVClock.put(
				intFinder.nextInt(), intFinder.nextInt());
		}
		return new VectorClock(remoteVClock);
	}
	
	public void update(VectorClock rClock) {
		synchronized (vclock) {
			if (rClock.vclock.size()!=this.vclock.size()) {
				return;
			} 
			Iterator it = rClock.vclock.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pairs = (Map.Entry)it.next();
		        Integer i = vclock.get(pairs.getKey());
		        if (i==null) {
		        	vclock.put((Integer) pairs.getKey(),
		        			(Integer) pairs.getValue());
		        } else
		        	vclock.put((Integer) pairs.getKey(), 
		        			Math.max((Integer) pairs.getValue(), 
		        			i));
		    }
		}
	}
	
	public void incrementClock() {
		synchronized (vclock) {
			Integer i = vclock.get(App.config.localId);
			if (i==null) {
				vclock.put(App.config.localId, 1);
				return;
			}
			vclock.put(App.config.localId, 
				1+i);
		}
	}
	
	public String toString() {
		StringBuffer ss = new StringBuffer("[");
		Iterator<Entry<Integer, Integer>> it = vclock.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry)it.next();
			ss.append("<" + pair.getKey() + "," + pair.getValue() + ">");
		}
		ss.append("]");
		return ss.toString();
	}
}

