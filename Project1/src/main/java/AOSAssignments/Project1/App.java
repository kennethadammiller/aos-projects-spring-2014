package AOSAssignments.Project1;

import org.zeromq.ZMQ;


public class App 
{
	public static Config config;
	public static ZMQ.Context context;
	//receives the identity in the args
	public static void main(String args[]) {
		context = ZMQ.context(1);
		if (args.length<1) {
			System.err.println("not the correct arguments");
			return;
		}
		int id = Integer.parseInt(args[0]);
		config = new Config(id, "config.xml");
		config.localId=id;
		
	    TCPReceiver receiver= new TCPReceiver();
	    receiver.start();
	    
	    TCPSender sender = new TCPSender();
	    sender.start();
	    try {
			sender.join();
			receiver.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	    config.flush();
	    config.close();
	    context.term();
	}
}
