package AOSAssignments.Project1;

import java.io.*;
import java.net.*;
import java.util.HashMap;

import org.zeromq.ZMQ;




public class TCPReceiver extends Thread {
	
	
	public TCPReceiver() {
	}
	
    public void run() {
		//TODO open line in, announce that you're ready to every other process
    	ZMQ.Poller items = new ZMQ.Poller (1);
    	App.config.writeOut("entered TCPReceiver\n");
		ZMQ.Socket receiver = App.context.socket(ZMQ.PULL);
		receiver.bind(
				App.config.localUrl + ":" + App.config.localPort);
		items.register(receiver, ZMQ.Poller.POLLIN);
		int remainingNeighbors=App.config.neighbors.size();
		while (remainingNeighbors>0) {
			items.poll();
			if (items.pollin(0)) {
				try {
					String message = new String(receiver.recv(), "UTF-8");
					if (isDoneMessage(message)) {
						remainingNeighbors--;
					}
					String[] idAndClock = parseMessage(message);
					VectorClock remoteClock = VectorClock.
							constructFromString(idAndClock[1]);
					App.config.writeOut("received message from " + idAndClock[0] 
							+ " with clock: " + remoteClock + "\n");
					App.config.writeOut("message received: " + message + "\n"); 
					VectorClock.getVectorClock().update(remoteClock);
					VectorClock.getVectorClock().incrementClock();
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
		}
		App.config.writeOut(
			"received final special message; exiting with vclock: "
			+ VectorClock.getVectorClock() + "\n");
		receiver.close();
	}
    
    public String[] parseMessage(String message) {
    	return message.split("[|]");
    }
    
    public boolean isDoneMessage(String message) {
    	if (parseMessage(message)[1].equals("done"))
    		return true;
    	return false;
    }
		
    	//TODO continually receive items, updating vectorclock each time.
    	//TODO await receiving "done" from every other node to quit. 
    		//Close connections when receiving done. 
}
