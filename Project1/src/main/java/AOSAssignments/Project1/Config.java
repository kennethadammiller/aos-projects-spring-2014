package AOSAssignments.Project1;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class Config {
	public int nodeCount;
	public String localUrl;
	public int localPort;
	public int localId;
	public List<Node> neighbors;
	public BufferedWriter out;
	//TODO setup the vector clock
	public class Node {
		int id;
		String url;
		int port;
		public Node(int _id, String _url, int _port) {
			id=_id;
			url=_url;
			port=_port;
		}
	}
	
	public synchronized void writeOut(String toWrite) {
		try {
			out.write(toWrite);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void flush() {
		try {
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void close() {
		try {
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Config(int id, String xmlFile) {
		localId = id;
		try {
			out = new BufferedWriter(new FileWriter("log" + id + ".out"));
		} catch (IOException e2) {
			e2.printStackTrace();
		}
	    File file = new File("config.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(file);
			doc.getDocumentElement().normalize();
			NodeList nodeC = doc.getElementsByTagName("nodeCount");
			neighbors = new LinkedList<Node>();
			Element nodeCountEle = (Element) doc.getElementsByTagName("nodeCount").item(0);
			nodeCount = Integer.parseInt(nodeCountEle.getAttribute("value"));
			NodeList nList = doc.getElementsByTagName("node");
			
			for (int i=0; i<nList.getLength();i++) {
				Element e = (Element) nList.item(i);
				int currentId = Integer.parseInt(e.getAttribute("nodeId"));
				String url = e.getAttribute("url");
				int port = Integer.parseInt(e.getAttribute("portNum"));
				
				if (localId == currentId) {
					localUrl = url;
					localPort = port;
				} else {
					neighbors.add(new Node(currentId, url, port));
				}
			}
		} catch (ParserConfigurationException e1) {
			e1.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

