package AOSAssignments.Project1;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.zeromq.*;


public class TCPSender extends Thread {
	private HashMap<Integer, Destination> neighborsAwaitingMessage; 
	
	public TCPSender() {
		//Maps Id's to their respective socket and their respective list of wait times.
		neighborsAwaitingMessage = new HashMap<Integer, Destination>();
	}
	
	public class Destination {
		public ZMQ.Socket sock;
		public LinkedList<Long> waitTimes;
		public int id;
		public Destination(ZMQ.Socket _sock, LinkedList<Long> times, int _id) {
			sock = _sock;
			waitTimes = times;
			id = _id;
		}
	}
	
	private LinkedList<Long> generateRandomWaitTimes() {
		LinkedList<Long> toRet = new LinkedList<Long>();
		Random r = new Random();
		int minCount=1;
		int maxCount=6;
		Long minWait = 25L * 100L;
		Long maxWait = 1L * 1000L;
		int count = minCount + r.nextInt(maxCount);
		for (int i=0;i<count;i++) {
			toRet.add( minWait +  Math.abs(r.nextLong()%maxWait));
		}
		return toRet;
	}
	
	//Create sockets to each of the respective destinations recorded in the config, and  
	//then send messages to it, waiting random short times between sending and waiting
	//pick a node at random to talk to and send it your id and the current vector clock
    public void run() {
		String message;
		App.config.writeOut("entered TCPSender\n");
		for (int i=0;i<App.config.neighbors.size();i++) {
			App.config.writeOut("creating Socket to neighbor " + 
					App.config.neighbors.get(i).id + "\n");
			ZMQ.Socket sender = App.context.socket(ZMQ.PUSH);
			sender.connect(
				App.config.neighbors.get(i).url + 
				":" + App.config.neighbors.get(i).port);
			neighborsAwaitingMessage.put(App.config.neighbors.get(i).id, 
					new Destination(sender, generateRandomWaitTimes(), 
							App.config.neighbors.get(i).id));
		}
		while (!neighborsAwaitingMessage.isEmpty()) {
			Random        random    = new Random();
			List<Integer> keys      = new ArrayList<Integer>(
					neighborsAwaitingMessage.keySet());
			Integer       randomKey = keys.get( random.nextInt(keys.size()) );
			ZMQ.Socket    socket     = neighborsAwaitingMessage.get(randomKey).sock;
			if (!neighborsAwaitingMessage.get(randomKey).waitTimes.isEmpty()) {
				try {
					Long sleeptime = neighborsAwaitingMessage.
							get(randomKey).waitTimes.pop();
					App.config.writeOut("TCPSender sleeping for " + sleeptime + "\n");
					Thread.sleep(sleeptime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				message = App.config.localId +"|"+ VectorClock.getVectorClock();
				App.config.writeOut("sending message to node " + 
				neighborsAwaitingMessage.get(randomKey).id + " randomKey: "  + randomKey
						 + "\n");
				VectorClock.getVectorClock().incrementClock();
				App.config.writeOut("incrementing clock before sending: " 
						+ VectorClock.getVectorClock() + "\n");
				socket.send(message);
			}
			else {
				App.config.writeOut("done with node " + 
					neighborsAwaitingMessage.get(randomKey).id + "\n");
				neighborsAwaitingMessage.remove(randomKey);
				socket.send(App.config.localId + "|" + "done");
				socket.close();
			}
		}
	}
}

