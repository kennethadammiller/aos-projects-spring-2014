#!/bin/bash
fileName=SOME_FILE
SSH_KEY=YOUR_KEY
mvn clean install
zip -r $fileName *
scp -2 -i $SSH_KEY $fileName $utd_id@cs1.utdallas.edu: && rm $fileName
