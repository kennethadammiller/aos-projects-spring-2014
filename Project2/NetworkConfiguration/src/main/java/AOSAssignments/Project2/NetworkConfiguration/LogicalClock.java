package AOSAssignments.Project2.NetworkConfiguration;

public class LogicalClock {
	private long clock;
	public LogicalClock() {
		clock=0L;
	}
	public void receiveMessage(long seq) {
		if (clock <= seq) {
			clock = seq+1;
		} else
			clock++;
	}
	
	public void event() {
		clock++;
	}
	
	public long getClock() {
		return clock;
	}
}
