package AOSAssignments.Project2.NetworkConfiguration;


import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;


public class Config {
	private static Config config;
	public static int DMEListenerPort = 6767;
	private int nodeCount;
	public String localUrl;
	public String killsigID = "killsig";
	public int localPort;
	private int localId;
	protected List<Node> neighbors;
	private static ZContext context;
	private ZMQ.Socket killSock;
	private LogicalClock lClock;
	private PrintWriter DMELog, ComLog;
	
	private int numSockets=0;
	public class Node {
		int id;
		String url;
		int port;
		public Node(int _id, String _url, int _port) {
			id=_id;
			url=_url;
			port=_port;
		}
	}
	
	private void init(int _id) {
		localId = _id;
		try {
			DMELog = new PrintWriter(new BufferedWriter(
					new FileWriter("DMELog" + localId + ".txt")));
			ComLog = new PrintWriter(new BufferedWriter(
					new FileWriter("ComLog" + localId + ".txt")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		init();
	}
	
	public ZMQ.Socket createSocket(int type) {
		return context.createSocket(type);
	}
	
	public void destroySocket(ZMQ.Socket sock) {
		context.destroySocket(sock);
	}
	
	private void init() {
		neighbors = new LinkedList<Node>();
		lClock = new LogicalClock();
		context = new ZContext();
		context.setLinger(-1);
		killSock = context.createSocket(ZMQ.PUB);
		killSock.bind("inproc://" + killsigID);
	}
	
	private Config() {
		init();
	}
	
	public void initConfig(int _id) {
		init(_id);
		initNeighbors();
	}
	
	public static Config getConfig() {
		if (config==null)
			config = new Config();
		return config;
	}
	
	public int getID() {
		return localId;
	}
	
	public void setID(int ID) {
		localId = ID;
		try {
			DMELog = new PrintWriter(new BufferedWriter(
					new FileWriter("DMELog" + localId + ".txt")));
			ComLog = new PrintWriter(new BufferedWriter(
					new FileWriter("ComLog" + localId + ".txt")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (neighbors.size()==0) {
			initNeighbors();
		}
	}
	
	public void writeToComLogFile(String toLog) {
		ComLog.write(toLog);
	}
	
	public void writeToDMELogFile(String toLog) {
		DMELog.write(toLog);
	}
	
	public HashMap<Integer, String> getNeighbors() {
		HashMap<Integer, String> toRet = new HashMap<Integer, String>();
		for (Node n : neighbors) {
			toRet.put(n.id, n.url + ":" + n.port);
		}
		return toRet;
	}
	
	public void close() {
		//TODO send killsig
		ComLog.flush();
		DMELog.flush();
		ComLog.close();
		DMELog.close();
		shutDown();
		config.destroySocket(killSock);
		context.destroy();
		config=null;
	}
	
	public void shutDown()  {
		killSock.send("");
	}
	
	public Config(int id) {
		init(id);
	    initNeighbors();
	}
	
	private void initNeighbors() {
		File file = new File("../config.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		int count = 0;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(file);
			doc.getDocumentElement().normalize();
			NodeList nodeC = doc.getElementsByTagName("nodeCount");
			Element nodeCountEle = (Element) doc.getElementsByTagName("nodeCount").item(0);
			nodeCount = Integer.parseInt(nodeCountEle.getAttribute("value"));
			NodeList nList = doc.getElementsByTagName("node");
			
			for (int i=0; i<nList.getLength();i++) {
				Element e = (Element) nList.item(i);
				int currentId = Integer.parseInt(e.getAttribute("nodeId"));
				String url = e.getAttribute("url");
				int port = Integer.parseInt(e.getAttribute("portNum"));
				
				if (localId == currentId) {
					localUrl = url;
					localPort = port;
				} 
				neighbors.add(new Node(currentId, url, port));
				count++;
			}
			nodeCount = count;
		} catch (ParserConfigurationException e1) {
			e1.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void event() {
		lClock.event();
	}
	
	public long getClock() {
		return lClock.getClock();
	}
	
	public void updateClock(long processClock) {
		lClock.receiveMessage(processClock);
	}
}

