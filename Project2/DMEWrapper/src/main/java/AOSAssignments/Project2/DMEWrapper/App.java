package AOSAssignments.Project2.DMEWrapper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.zeromq.ZMQ;

import AOSAssignments.Project2.DMEImplementation.DMEImplementation;
import AOSAssignments.Project2.NetworkConfiguration.Config;


public class App 
{
	public static Config config;
	
	public static void main(String args[]) {
		if (args.length<1) {
			System.err.println("not the correct arguments");
			return;
		}
		int id = Integer.parseInt(args[0]);
		config =  Config.getConfig();
		config.setID(id);

		File logFile=new File("CriticalSectionLog.txt");

	    BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(logFile));
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(-1);
		}
	    
		DMEImplementation dmeService = new DMEImplementation();
		for (int i=0;i<5;i++) {
			dmeService.csenter();
			String timeLog = new SimpleDateFormat("HH:mm:ss").format(
					Calendar.getInstance().getTime());
			try {
				writer.write("Node with ID: " + 
						Config.getConfig().getID() + " entering CS - " + timeLog);
			} catch (IOException e) {
				e.printStackTrace();
			}
			dmeService.csleave();
		}
		
		dmeService.close();
		config.close();
	}
}
