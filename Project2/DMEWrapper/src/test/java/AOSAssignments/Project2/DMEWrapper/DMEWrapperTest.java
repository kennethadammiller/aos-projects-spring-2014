package AOSAssignments.Project2.DMEWrapper;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.lib.concurrent.Synchroniser;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.zeromq.ZMQ;

import AOSAssignments.Project2.DMEImplementation.Connection;
import AOSAssignments.Project2.DMEImplementation.CoreCommunicator;
import AOSAssignments.Project2.DMEImplementation.DMEImplementation;
import AOSAssignments.Project2.DMEImplementation.TreeQuorum;
import AOSAssignments.Project2.NetworkConfiguration.Config;

public class DMEWrapperTest {
	private Config c;
	protected static Mockery context = null;
	protected ZMQ.Socket out, in;
	private String cs_log_filename = "CS_log.txt";
	
	@Before
	public void setUp() {
		context = new Mockery() {{
			setThreadingPolicy(new Synchroniser());
	        setImposteriser(ClassImposteriser.INSTANCE);
	    }};
		c = Config.getConfig();
		c.setID(3);
		c.localUrl = "tcp://localhost";
	}
	
	@After
	public void tearDown() { 
		c.close();
	}
	
	private boolean passed = false;
	@Test
	public void testSocketWorks() {
		out = c.createSocket(ZMQ.PUSH);
		in  = c.createSocket(ZMQ.PULL);
		String s = "blah";
		out.bind("inproc://testOutToIn");
		in.connect("inproc://testOutToIn");
		Thread t1 = new Thread(new Runnable() { public void run() { 
			if (in.recvStr().equals("blah"))
				passed = true;
			c.destroySocket(in);
		}});
		t1.start();
		out.send(s);
		c.destroySocket(out);
		try {
			t1.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		assertTrue("unexpected message!" , passed);
	}
	
	public void initCSLeaveDisabledTests(DMELeaveDisabled leaveDisabled) {
		if (leaveDisabled.isInCriticalSection()) {
			try {
				PrintWriter out = 
					new PrintWriter(
						new BufferedWriter(new FileWriter(cs_log_filename))); 
				Calendar cal = Calendar.getInstance();
		    	cal.getTime();
		    	SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			    out.println("Node: " + Config.getConfig().getID() + 
			    		" entered CS @: " + sdf.format(cal.getTime()));
			    out.flush();
			    out.close();
			}catch (IOException e) {
			    e.printStackTrace();
			}
		}
	}
	
	@Test
	public void testCSLeaveDisabledQuorumSuccess() {
		Thread t1 = new Thread(new Runnable() { public void run() { 
			ZMQ.Socket contrivedResponse = c.createSocket(ZMQ.PUSH);
			contrivedResponse.connect("tcp://localhost:" + c.localPort);
			for (int i = 0; i< 3; i++) {
				contrivedResponse.send("LOCKED 999");
			}
			c.destroySocket(contrivedResponse);
		}});
		t1.start();
		DMELeaveDisabled leaveDisabled = new DMELeaveDisabled();
		leaveDisabled.csenter();
		try {
			t1.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		initCSLeaveDisabledTests(leaveDisabled);
		File cs_log = new File(cs_log_filename);
		assertTrue("CS_log.txt does not exist!", (cs_log).exists());
		BufferedReader br;
		int count=0;
		try {
			br = new BufferedReader(new FileReader(cs_log));
			while (br.readLine() != null) {
			   count++;
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertTrue("more than one line in the file!", count==1);
		//TODO assert there is only one line in the CS_log.txt file
		cs_log.delete();
		leaveDisabled.close();
	}
	
	//@Test
	public void testCSLeaveDisabledQuorumFailure() {
		DMELeaveDisabled leaveDisabled = new DMELeaveDisabled(31);
		leaveDisabled.csenter();
		initCSLeaveDisabledTests(leaveDisabled);
		assertTrue("Log file should not exist; should not be in ", 
				!(new File(cs_log_filename).exists()));
		leaveDisabled.close();
	}

	protected class DMELeaveDisabled extends DMEImplementation {
		@Override
		public void csleave() {
			
		}
		
		public DMELeaveDisabled(int maxTimeout) {
			super(new MockedConnectionCoreCommunicator(maxTimeout));
		}
		
		public DMELeaveDisabled() {
			super(new MockedConnectionCoreCommunicator());
		}
	}
	
	protected class MockedConnectionCoreCommunicator extends CoreCommunicator {
		public MockedConnectionCoreCommunicator(int _maxTimeout) {
			maxTimeout = _maxTimeout;
		}
		
		public MockedConnectionCoreCommunicator() {
		}
		
		public TreeQuorum getTreeQuorum() {
			return MockedSingletonTreeQuorum.getTreeQuorum();
		}
	}

	protected static class MockedSingletonTreeQuorum {
		private static TreeQuorum tQ = null;
		private MockedSingletonTreeQuorum() {
		}
		
		public static TreeQuorum getTreeQuorum() {
            if (tQ == null) {
            	List<Map.Entry<Integer, Connection>> tQMockedNeighbors = 
            			new LinkedList<Map.Entry<Integer, Connection>>();
            	final Connection conn = context.mock(Connection.class);
            	context.checking(new Expectations() {{
            		atLeast(1).of(conn).grantsPermission();
            		will(returnValue(true));
            		allowing(conn).hasID(with(any(Integer.class)));
            		allowing(conn).sendRequestMessage();
            		atLeast(1).of(conn).sendReleaseMessage();
            		atLeast(1).of(conn).close();
            	}});
            	for (int i = 1;i<= 7;i++) {
            		tQMockedNeighbors.add(new AbstractMap.
            				SimpleEntry<Integer, Connection>(i, conn));
            	}
                tQ = new TreeQuorum(tQMockedNeighbors);
            }
            return tQ;
        }
	}
	
}
