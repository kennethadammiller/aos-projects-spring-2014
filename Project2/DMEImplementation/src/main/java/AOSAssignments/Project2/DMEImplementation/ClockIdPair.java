package AOSAssignments.Project2.DMEImplementation;

public class ClockIdPair implements Comparable<ClockIdPair> {
	private long clockValue;
	private int ID;
	
	public ClockIdPair(long clockValue, int ID) {
            this.clockValue = clockValue;
            this.ID = ID;
	}
	
	public int compareTo(ClockIdPair o) {
		if (clockValue==o.getClockValue())
			return 0;
		else if (clockValue<o.getClockValue())
			return -1;
		else // if (ID>o) 
			return 1;
	}
	
	public long getClockValue() {
		return clockValue;
	}
	
	public int getID() {
		return ID;
	}
}
