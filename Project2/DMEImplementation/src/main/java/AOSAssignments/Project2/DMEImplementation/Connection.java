package AOSAssignments.Project2.DMEImplementation;

import org.zeromq.ZMQ;

import AOSAssignments.Project2.NetworkConfiguration.Config;
import java.util.Map.Entry;

public class Connection {

    ZMQ.Socket sock;
    String urlAndPort;
    int remoteID;
    private Config c;

    public Connection(ZMQ.Socket _sock) {
    	c = Config.getConfig();
        sock = _sock;
    }

	public Connection(String _urlAndPort, int ID) {
		c = Config.getConfig();
		sock = c.createSocket(ZMQ.PUSH);
		urlAndPort = _urlAndPort;
		remoteID = ID;
		sock.connect(urlAndPort);
	}
	
	public boolean hasID(int id) {
		if (id==remoteID) {
			return true;
		}
		return false;
	}

	public void close() {
		c.destroySocket(sock);
	}

    public void sendRequestMessage() {
    	sendMessage("REQUEST " + c.getID() + " "
                + c.getClock(), ZMQ.NOBLOCK);
    }

    public void sendRelinquishMessage() {
    	sendMessage("RELINQUISH " + c.getID());
    }

    public void sendReleaseMessage() {
    	sendMessage("RELEASE " + c.getID()); 
    }

    public void sendLockedMessage() {
    	sendMessage("LOCKED " + c.getID());
    }

    public void sendFailedMessage() {
    	sendMessage("FAILED " + c.getID());
    }

    public void sendInquireMessage() {
    	sendMessage("INQUIRE " + c.getID());
    }
    
    private void sendMessage(String toSend) {
    	c.writeToComLogFile(toSend);
    	sock.send(toSend);
    }
    
    private void sendMessage(String toSend, int flags) {
    	c.writeToComLogFile(toSend);
    	sock.send(toSend, flags);
    }

    public boolean grantsPermission() {
        return true;
    }
}
