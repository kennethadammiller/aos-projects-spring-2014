

package AOSAssignments.Project2.DMEImplementation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Socket;

import AOSAssignments.Project2.NetworkConfiguration.Config;

public class TreeQuorum {

    protected AvlTree tree = null;

    public TreeQuorum() {
    	tree = new AvlTree();
    	HashMap<Integer, String> neighbors= Config.getConfig().getNeighbors();
    	for (Entry<Integer, String> n : neighbors.entrySet()) 
    		tree.insert(n.getKey(), new Connection(n.getValue(), n.getKey()));
    	tree.recursiveBalance(tree.root);
    }
    
    public TreeQuorum(List<Entry<Integer, Connection>> neighbors) {
        setup(neighbors);
    }
    
    public Connection find(int ID) {
    	return tree.find(ID).conn;
    }
    
    public TreeQuorum(HashMap<Integer, Connection> neighbors) {
    	tree = new AvlTree();
    	for (Entry<Integer, Connection> e : neighbors.entrySet()) {
    		tree.insert(e.getKey(), e.getValue());
    	}
    	tree.recursiveBalance(tree.root);
    }

    public void setup(List<Entry<Integer, Connection>> neighbors) {
        tree = new AvlTree(neighbors);
        for (Entry<Integer, Connection> e : neighbors) {
        	tree.insert(e.getKey(), e.getValue());
        }
        tree.recursiveBalance(tree.root);
    }
    
    public void close() {
    	for (AvlNode n : tree.inorder()) {
    		n.conn.close();
    	}
    	tree=null;  //Deallocate tree
    }

    private HashMap<Integer, Connection> getQuorum(AvlNode node) {
        HashMap<Integer, Connection> result = new HashMap<Integer, Connection>();
        HashMap<Integer, Connection> left = new HashMap<Integer, Connection>();
        HashMap<Integer, Connection> right = new HashMap<Integer, Connection>();

        if (node==null) { // if the tree is empty
            return result;
        } else if (node.conn.grantsPermission()) {
            result.put(node.key, node.conn);
            if (node.key > Config.getConfig().getID()) {
	            left = getQuorum(node.left);
	            if (left != null && !left.isEmpty()) {
	                return UnionHashMaps(result, left); // node + left children
	            }
            } /* else if (left==null) */
            else if (node.key < Config.getConfig().getID()) {
	            right = getQuorum(node.right);
	            if (right != null && !right.isEmpty()) {
	                return UnionHashMaps(result, right); // node + right children
	            }
            } /* else if (right==null)*/
            else if (node.key==Config.getConfig().getID()) {
            	//check to see if at leaf
            	if (node.left==null && node.right==null) {
            		return result;
            	} else {
            		return getRandomQuorum(node);
            	}
            }
            //TODO throw exception
            return null;
        } else {
            left = getQuorum(node.left);
            right = getQuorum(node.right);
            if (left == null || right == null) {
                //System.err.println("Unsuccessful in establishing a quorum");
            	//TODO throw exception
                return null;
            } else {
                return UnionHashMaps(left, right);
            }
        }

    }
    
    private HashMap<Integer, Connection> getRandomQuorum(AvlNode node) {
    	HashMap<Integer, Connection> result = new HashMap<Integer, Connection>();
        HashMap<Integer, Connection> left = new HashMap<Integer, Connection>();
        HashMap<Integer, Connection> right = new HashMap<Integer, Connection>();
        if (node==null) { // if the tree is empty
            return result;
        } else {
        	result.put(node.key, node.conn);
        	if (Math.random()>0.5) {
        		return UnionHashMaps(result, getRandomQuorum(node.left));
        	} else { //if (coin.get(0)==1) {
        		return UnionHashMaps(result, getRandomQuorum(node.right));
        	} 
        }
    }
    
    public HashMap<Integer, Connection> getQuorum() {
    	if (tree.find(Config.getConfig().getID())==null) {
    		return getRandomQuorum(tree.root);
    	}
    	else {
    		return getQuorum(tree.root);
    	}
    }
    
    
    private HashMap<Integer, Connection> UnionHashMaps(HashMap<Integer, Connection> a, HashMap<Integer, Connection> b) {
        if (a != null && b != null){
            if (a.isEmpty() && b.isEmpty()){
                return a;
            } 
            else if (a.isEmpty()){
                return b;
            } else if (b.isEmpty()){
                return a;
            } else {
                HashMap<Integer, Connection> union = new HashMap<Integer, Connection>(a);
                union.putAll(b);
                return union;
            }
        } else {
            return null;
        }

    }

//	public HashMap<Integer, ZMQ.Socket> getQuorum(int key) {
//		return tree.traverse(new TraversalOperation() {
//			public AvlNode inTraversalOp(AvlNode x,
//					HashMap<Integer, Socket> connList) {
//				//TODO if the sock of the current node is valid, then return it.
//				//sock is bad; return null for traversal
//				if (isValid(x))
//					return x;
//				return null;
//				//sock is bad; return null for traversal\
//			}
//		});
//	}

    private interface TraversalOperation {

        public AvlNode inTraversalOp(AvlNode x, HashMap<Integer, Connection> conns);
    }

    protected class AvlTree {
    	
    	public AvlTree() {
    		
    	}

        private AvlTree(List<Entry<Integer, Connection>> all_nodes) {
            for (Entry<Integer, Connection> e : all_nodes) {
            	insert(e.getKey(), e.getValue());
            }
        }
        
        protected AvlNode find(int nodeID) {
        	return find(nodeID, root);
        }
        
        private AvlNode find(int nodeID, AvlNode curRoot) {
            if (curRoot == null) {
                return null;
            } else {
                if (curRoot.key > nodeID) {
                	return find(nodeID, curRoot.left);
                } else if (curRoot.key < nodeID) {
                	return find(nodeID, curRoot.right);
                } else if (curRoot.key == nodeID) {
                    return curRoot;
                }
            }
            return null;
        }

        protected HashMap<Integer, Connection> traverse(TraversalOperation op) {
            HashMap<Integer, Connection> conns = new HashMap<Integer, Connection>();
            AvlNode x = op.inTraversalOp(this.root, conns);
            if (x != null) {
                conns.put(x.key, x.conn);
            } else {
                //TODO handle error condition; either sock is just bad or quorum cannot be formed
            }
            return traverse(op, x, conns);
        }

        protected HashMap<Integer, Connection> traverse(TraversalOperation op, 
        		AvlNode x, HashMap<Integer, Connection> conns) {
            x = op.inTraversalOp(x, conns);
            if (x != null) {
                conns.put(x.key, x.conn);
            } else {
                //TODO handle error condition here; either sock is just bad or quorum cannot be formed
            }
			//if have finished traversal {
            //return conns;
            //} else {
            //TODO decide which way to go somehow
            //return traverse(op, )
            //}
            return null;
        }

        protected AvlNode root; // the root node

        /**
         * *************************** Core Functions ***********************************
         */
        /**
         * Add a new element with key "k" into the tree.
         *
         * @param k The key of the new node.
         */
        private void insert(int k, ZMQ.Socket sock) {
            // create new node
            AvlNode n = new AvlNode(k);
            n.conn = new Connection(sock);
            // start recursive procedure for inserting the node
            insertAVL(this.root, n);
        }

        private void insert(int k, Connection conn) {
        	AvlNode n = new AvlNode(k);
        	n.conn = conn;
        	insertAVL(this.root, n);
        }
        /**
         * Recursive method to insert a node into a tree.
         *
         * @param p The node currently compared, usually you start with the
         * root.
         * @param q The node to be inserted.
         */
        private void insertAVL(AvlNode p, AvlNode q) {
			// If node to compare is null, the node is inserted. If the root is
            // null, it is the root of the tree.
            if (p == null) {
                this.root = q;
            } else {

                // If compare node is smaller, continue with the left node
                if (q.key < p.key) {
                    if (p.left == null) {
                        p.left = q;
                        q.parent = p;

                        // Node is inserted now, continue checking the balance
                        recursiveBalance(p);
                    } else {
                        insertAVL(p.left, q);
                    }

                } else if (q.key > p.key) {
                    if (p.right == null) {
                        p.right = q;
                        q.parent = p;

                        // Node is inserted now, continue checking the balance
                        recursiveBalance(p);
                    } else {
                        insertAVL(p.right, q);
                    }
                } else {
                    // do nothing: This node already exists
                }
            }
        }

        /**
         * Check the balance for each node recursivly and call required methods
         * for balancing the tree until the root is reached.
         *
         * @param cur : The node to check the balance for, usually you start
         * with the parent of a leaf.
         */
        protected void recursiveBalance(AvlNode cur) {

            // we do not use the balance in this class, but the store it anyway
            setBalance(cur);
            int balance = cur.balance;

            // check the balance
            if (balance == -2) {

                if (height(cur.left.left) >= height(cur.left.right)) {
                    cur = rotateRight(cur);
                } else {
                    cur = doubleRotateLeftRight(cur);
                }
            } else if (balance == 2) {
                if (height(cur.right.right) >= height(cur.right.left)) {
                    cur = rotateLeft(cur);
                } else {
                    cur = doubleRotateRightLeft(cur);
                }
            }

            // we did not reach the root yet
            if (cur.parent != null) {
                recursiveBalance(cur.parent);
            } else {
                this.root = cur;
            }
        }

        /**
         * Removes a node from the tree, if it is existent.
         */
        private void remove(int k) {
            // First we must find the node, after this we can delete it.
            removeAVL(this.root, k);
        }

        /**
         * Finds a node and calls a method to remove the node.
         *
         * @param p The node to start the search.
         * @param q The KEY of node to remove.
         */
        private void removeAVL(AvlNode p, int q) {
            if (p == null) {
                return;
            } else {
                if (p.key > q) {
                    removeAVL(p.left, q);
                } else if (p.key < q) {
                    removeAVL(p.right, q);
                } else if (p.key == q) {
                    // we found the node in the tree.. now lets go on!
                    removeFoundNode(p);
                }
            }
        }

        /**
         * Removes a node from a AVL-Tree, while balancing will be done if
         * necessary.
         *
         * @param q The node to be removed.
         */
        private void removeFoundNode(AvlNode q) {
            AvlNode r;
            // at least one child of q, q will be removed directly
            if (q.left == null || q.right == null) {
                // the root is deleted
                if (q.parent == null) {
                    this.root = null;
                    q = null;
                    return;
                }
                r = q;
            } else {
                // q has two children --> will be replaced by successor
                r = successor(q);
                q.key = r.key;
            }

            AvlNode p;
            if (r.left != null) {
                p = r.left;
            } else {
                p = r.right;
            }

            if (p != null) {
                p.parent = r.parent;
            }

            if (r.parent == null) {
                this.root = p;
            } else {
                if (r == r.parent.left) {
                    r.parent.left = p;
                } else {
                    r.parent.right = p;
                }
                // balancing must be done until the root is reached.
                recursiveBalance(r.parent);
            }
            r = null;
        }

        /**
         * Left rotation using the given node.
         *
         *
         * @param n The node for the rotation.
         *
         * @return The root of the rotated tree.
         */
        private AvlNode rotateLeft(AvlNode n) {

            AvlNode v = n.right;
            v.parent = n.parent;

            n.right = v.left;

            if (n.right != null) {
                n.right.parent = n;
            }

            v.left = n;
            n.parent = v;

            if (v.parent != null) {
                if (v.parent.right == n) {
                    v.parent.right = v;
                } else if (v.parent.left == n) {
                    v.parent.left = v;
                }
            }

            setBalance(n);
            setBalance(v);

            return v;
        }

        /**
         * Right rotation using the given node.
         *
         * @param n The node for the rotation
         *
         * @return The root of the new rotated tree.
         */
        private AvlNode rotateRight(AvlNode n) {

            AvlNode v = n.left;
            v.parent = n.parent;

            n.left = v.right;

            if (n.left != null) {
                n.left.parent = n;
            }

            v.right = n;
            n.parent = v;

            if (v.parent != null) {
                if (v.parent.right == n) {
                    v.parent.right = v;
                } else if (v.parent.left == n) {
                    v.parent.left = v;
                }
            }

            setBalance(n);
            setBalance(v);

            return v;
        }

        /**
         *
         * @param u The node for the rotation.
         * @return The root after the double rotation.
         */
        private AvlNode doubleRotateLeftRight(AvlNode u) {
            u.left = rotateLeft(u.left);
            return rotateRight(u);
        }

        /**
         *
         * @param u The node for the rotation.
         * @return The root after the double rotation.
         */
        private AvlNode doubleRotateRightLeft(AvlNode u) {
            u.right = rotateRight(u.right);
            return rotateLeft(u);
        }

        /**
         * *************************** Helper Functions ***********************************
         */
        /**
         * Returns the successor of a given node in the tree (search
         * recursivly).
         *
         * @param q The predecessor.
         * @return The successor of node q.
         */
        private AvlNode successor(AvlNode q) {
            if (q.right != null) {
                AvlNode r = q.right;
                while (r.left != null) {
                    r = r.left;
                }
                return r;
            } else {
                AvlNode p = q.parent;
                while (p != null && q == p.right) {
                    q = p;
                    p = q.parent;
                }
                return p;
            }
        }

        /**
         * Calculating the "height" of a node.
         *
         * @param cur
         * @return The height of a node (-1, if node is not existent eg. NULL).
         */
        private int height(AvlNode cur) {
            if (cur == null) {
                return -1;
            }
            if (cur.left == null && cur.right == null) {
                return 0;
            } else if (cur.left == null) {
                return 1 + height(cur.right);
            } else if (cur.right == null) {
                return 1 + height(cur.left);
            } else {
                return 1 + maximum(height(cur.left), height(cur.right));
            }
        }

        /**
         * Return the maximum of two integers.
         */
        private int maximum(int a, int b) {
            if (a >= b) {
                return a;
            } else {
                return b;
            }
        }

        /**
         * Only for debugging purposes. Gives all information about a node.
         *
         * @param n The node to write information about.
         */
        private void debug(AvlNode n) {
            int l = 0;
            int r = 0;
            int p = 0;
            if (n.left != null) {
                l = n.left.key;
            }
            if (n.right != null) {
                r = n.right.key;
            }
            if (n.parent != null) {
                p = n.parent.key;
            }

            System.out.println("Left: " + l + " Key: " + n + " Right: " + r
                    + " Parent: " + p + " Balance: " + n.balance);

            if (n.left != null) {
                debug(n.left);
            }
            if (n.right != null) {
                debug(n.right);
            }
        }

        private void setBalance(AvlNode cur) {
            cur.balance = height(cur.right) - height(cur.left);
        }

        /**
         * Calculates the Inorder traversal of this tree.
         *
         * @return A Array-List of the tree in inorder traversal.
         */
        protected ArrayList<AvlNode> inorder() {
            ArrayList<AvlNode> ret = new ArrayList<AvlNode>();
            inorder(root, ret);
            return ret;
        }

        /**
         * Function to calculate inorder recursivly.
         *
         * @param n The current node.
         * @param io The list to save the inorder traversal.
         */
        protected void inorder(AvlNode n, ArrayList<AvlNode> io) {
            if (n == null) {
                return;
            }
            inorder(n.left, io);
            io.add(n);
            inorder(n.right, io);
        }
        
        protected ArrayList<AvlNode> preorder() {
        	ArrayList<AvlNode> ret = new ArrayList<AvlNode>();
        	preorder(root, ret);
        	return ret;
        }
        
        protected void preorder(AvlNode n, ArrayList<AvlNode> io) {
        	if (n==null) {
        		return;
        	}
        	io.add(n);
        	preorder(n.left, io);
        	preorder(n.right, io);
        }
        
        protected ArrayList<AvlNode> postorder() {
        	ArrayList<AvlNode> ret = new ArrayList<AvlNode>();
            postorder(root, ret);
            return ret;
        }
        
        protected void postorder(AvlNode n, ArrayList<AvlNode> io) {
        	if (n==null) {
        		return;
        	}
        	postorder(n.left, io);
        	postorder(n.right, io);
        	io.add(n);
        }
    }

    /**
     * Here is the AVL-Node class for Completenesse *
     */
    public class AvlNode {

        public AvlNode left;
        public AvlNode right;
        public AvlNode parent;
        public int key;
        public int balance;
        public Connection conn;

        public AvlNode(int k) {
            left = right = parent = null;
            balance = 0;
            key = k;
        }

        public String toString() {
            return "" + key;
        }

    }
}
