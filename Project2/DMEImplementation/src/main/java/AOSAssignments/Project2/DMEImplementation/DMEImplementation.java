package AOSAssignments.Project2.DMEImplementation;

import org.zeromq.ZMQ;

import AOSAssignments.Project2.DMEInterface.DistributedMutualExclusionInterface;
import AOSAssignments.Project2.NetworkConfiguration.Config;

public class DMEImplementation  implements DistributedMutualExclusionInterface {
	private ZMQ.Socket DMEReceiver, DMESender;
	private ZMQ.Socket cyclical_sender, cyclical_receiver;
	private CoreCommunicator cc;
	private Config c;

    public DMEImplementation() {
        init(new CoreCommunicator());
    }

	public DMEImplementation(int maxTimeout) {
    	init(new CoreCommunicator(maxTimeout));
    }
    
    public DMEImplementation(CoreCommunicator _cc) {
    	init(_cc);
    }

    protected static class SingletonTreeQuorum {

        private static TreeQuorum tQ = null;

        private SingletonTreeQuorum() {
        }

        public static TreeQuorum getTreeQuorum() {
            if (tQ == null) {
                tQ = new TreeQuorum();
            }
            return tQ;
        }
    }

	private void init(CoreCommunicator _cc) {
    	cc = _cc;
    	c = Config.getConfig();
    	DMESender = c.createSocket(ZMQ.PUSH);
        DMESender.bind("inproc://DMEIToCyclical");
        cyclical_sender = c.createSocket(ZMQ.PUSH);
    	cyclical_sender.bind("inproc://CyclicalToDMEI");
    	cc.setSender(cyclical_sender);
    	
    	DMEReceiver = c.createSocket(ZMQ.PULL); 
        DMEReceiver.connect("inproc://CyclicalToDMEI");
        cyclical_receiver = c.createSocket(ZMQ.PULL);
    	cyclical_receiver.connect("inproc://DMEIToCyclical");
    	cc.setReceiver(cyclical_receiver);
    	cc.start();
    }
    
    public void csenter() {
    	c.writeToDMELogFile("Requesting CSEnter");
        DMESender.send("ENTER");
        DMEReceiver.recv();
    }

    public void csleave() {
    	c.writeToDMELogFile("Leaving CS");
        DMESender.send("LEAVE");
    }
    
	public boolean isInCriticalSection() {
		return cc.isInCriticalSection();
	}

	public void close() {
		Config.getConfig().shutDown();
		try {
			cc.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		c.destroySocket(DMESender);
		c.destroySocket(DMEReceiver);
		cc.close();
	}
}
