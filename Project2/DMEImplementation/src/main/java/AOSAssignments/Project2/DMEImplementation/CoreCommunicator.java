package AOSAssignments.Project2.DMEImplementation;

import java.util.HashMap;
import java.util.PriorityQueue;

import org.zeromq.ZMQ;

import AOSAssignments.Project2.NetworkConfiguration.Config;

public class CoreCommunicator extends Thread {

    protected boolean locked = false;
    protected boolean isInCriticalSection = false;
    protected boolean isReceivedReplyForInquire = true;
    protected long lockedWithClock = Integer.MAX_VALUE;
    protected PriorityQueue<ClockIdPair> queue;
    protected int lockedTo = -1;
    protected int timeout = -1, maxTimeout;
    protected ZMQ.Socket cyclical_receiver, cyclical_sender,
            deathSock, outbound_receiver;
    protected HashMap<Integer, Connection> myQuorum;
    private Config c;
    private boolean isReceivedaFailedMessage = false;
    int receivedOKs = 0;

    public CoreCommunicator(int _maxTimeout) {
        maxTimeout = _maxTimeout;
        init();
    }

    public CoreCommunicator() {
        init();
    }

    public void setReceiver(ZMQ.Socket receiver) {
        cyclical_receiver = receiver;
    }

    public void setSender(ZMQ.Socket sender) {
        cyclical_sender = sender;
    }

    public TreeQuorum getTreeQuorum() {
    	TreeQuorum tQuorum = SingletonTreeQuorum.getTreeQuorum();
    	tQuorum.tree.recursiveBalance(tQuorum.tree.root);
        return tQuorum;
    }

    public void init() {
        queue = new PriorityQueue<ClockIdPair>();
        c = Config.getConfig();
        deathSock = c.createSocket(ZMQ.SUB);
        deathSock.connect("inproc://" + Config.getConfig().killsigID);
        deathSock.subscribe("".getBytes());

        outbound_receiver = c.createSocket(ZMQ.PULL);
        outbound_receiver.bind(c.localUrl + ":"
                + c.localPort);
        maxTimeout=-1;
    }

    // This function handles requests from remote sites to enter CS
    public void run() {
        // Status: In progress
        ZMQ.Poller incoming = new ZMQ.Poller(3);
        incoming.register(deathSock, ZMQ.Poller.POLLIN);
        incoming.register(outbound_receiver, ZMQ.Poller.POLLIN);
        incoming.register(cyclical_receiver, ZMQ.Poller.POLLIN);
        int receivedOKs = 0;
        boolean dead = false;
        boolean awaiting_response = false;
        int exponent = 4;
        while (!dead) {
            int num_sigs = -1;
            if (awaiting_response && timeout != -1) {
                num_sigs = incoming.poll(timeout * 1000);
            } else {
                num_sigs = incoming.poll();
            }
            if (num_sigs == 0) {
                timeout = timeout + (int) Math.pow(2, exponent);
                if (maxTimeout!=-1 && timeout > maxTimeout) {
                    releaseQuorum();
                    cyclical_sender.send("");
                    return; //return?
                }
                exponent++;
                continue;
            }
            if (incoming.pollin(0)) {
                dead = true;
                c.destroySocket(deathSock);
                c.destroySocket(outbound_receiver);
                c.destroySocket(cyclical_receiver);
                c.destroySocket(cyclical_sender);
                break;
            } else if (incoming.pollin(1)) {
                String receivedString = outbound_receiver.recvStr();
                String[] messagePart = receivedString.split(" ");

                int processID = Integer.parseInt(messagePart[1]);
                if (messagePart[0].equalsIgnoreCase("RELEASE")) {  //RELEASE ID
                    if (queue.isEmpty() && processID==lockedTo) {
                        locked = false;
                        lockedTo = -1;
                        lockedWithClock = Integer.MAX_VALUE;
                    } else {
                    	if (popAndLock()==null)
                    		continue;
                    }
                    isReceivedReplyForInquire = true;
                    // TODO: in progress
                } else if (messagePart[0].equalsIgnoreCase("REQUEST")) { //REQUEST ID CLOCK
                    long processClock = Long.parseLong(messagePart[2]);
                    if (!locked) { // if the process has not locked before
                        locked = true;
                        lockedTo = processID;
                        lockedWithClock = processClock;
                        if (myQuorum==null) 
                        	myQuorum = getTreeQuorum().getQuorum();
                        getTreeQuorum().find(processID).sendLockedMessage();
                    } else {
                        queue.add(new ClockIdPair(processClock, processID));
                        if (queue.peek().getClockValue() == processClock
                                && processClock < lockedWithClock) {
                        	if (doIsLockedToSelf()) 
                        		continue;
                        } else if (queue.peek().getClockValue()== processClock &&
                        		processClock == lockedWithClock) {
                        	if (lockedTo < processID)
                        		getTreeQuorum().find(processID).sendFailedMessage();
                        	else {
                        		if (doIsLockedToSelf())
                        			continue;
                        	}
                        } else { // There is at least one process that proceeds this one
                            getTreeQuorum().find(processID).sendFailedMessage();
                        }
                    }
                    c.updateClock(processClock);
                } else if (messagePart[0].equalsIgnoreCase("INQUIRE")) {
                	/**
                	 * if isReceivedaFailedMessage
                	 * 		sendRelinquish
                	 * setDeferred...
                	 * if an INQUIRE message has arrived
                	 * before it is known whether the node will succeed or fail to lock all its members, a
                	 * reply is deferred until this becomes known. If an INQUIRE message has arrived after 
                	 * the node has sent a RELEASE message, it is simply ignored.
                	 * cancelPossibleLockMessage(processID);  //evaluates to relinquish
                	 * setPendingReleaseUponLeaveCS(processID); //list to resolve
                	 * 
                	 */
                    if (isReceivedaFailedMessage) {
                        getTreeQuorum().find(processID).sendRelinquishMessage(); //TODO
                        receivedOKs = 0;
                        isReceivedaFailedMessage = false;
                    } else if (!isInCriticalSection)
                    	getTreeQuorum().find(processID).sendReleaseMessage();
                } else if (messagePart[0].equalsIgnoreCase("RELINQUISH")) {
                    int tempID = lockedTo;
                    long tempClock = lockedWithClock;
                    if (popAndLock()==null)
                    	continue;
                    else
                    	queue.add(new ClockIdPair(tempClock, tempID));
                    isReceivedReplyForInquire = true;
                } else if (messagePart[0].equalsIgnoreCase("FAILED")) {
                    isReceivedaFailedMessage = true;
                } else if (messagePart[0].equalsIgnoreCase("LOCKED")) {
                    exponent = 4;
                    receivedOKs++;
                    int expectedSize = getTreeQuorum().getQuorum().size();
                    if (getTreeQuorum().getQuorum().containsKey(c.getID())) {
                    	expectedSize -= 1;
                    }
                    if (receivedOKs == expectedSize) {
                        isInCriticalSection = true; 
                        awaiting_response=false;
                        receivedOKs = 0;
                        timeout = 30;
                        cyclical_sender.send("");
                    } continue;
                }
            } else if (incoming.pollin(2)) {
                String receivedStr = cyclical_receiver.recvStr();
                if (receivedStr.equalsIgnoreCase("ENTER")) {
                    myQuorum = getTreeQuorum().getQuorum();
                    locked = true;
                    lockedTo = c.getID(); 
                    lockedWithClock = c.getClock();
                    for (Connection conn : myQuorum.values()) {
                    	if (!conn.hasID(c.getID()))
                    		conn.sendRequestMessage();
                    }
                    Config.getConfig().event();
                    receivedOKs = 0;
                    awaiting_response = true;
                    timeout = 30;
                    continue;
                } else if (receivedStr.equalsIgnoreCase("LEAVE")) {
                	//TODO evaluate list of pending release messages
                    timeout = 30;
                    awaiting_response = false;
                    locked = false;
                    lockedTo=-1;  
                    lockedWithClock=Integer.MAX_VALUE;
                    releaseQuorum();
                    isInCriticalSection = false;
                    if (popAndLock()==null)
                    	continue;
                }
            }
        }
    }
    
    private boolean doIsLockedToSelf() {
    	if (isReceivedReplyForInquire && lockedTo!=c.getID()) {
            getTreeQuorum().find(lockedTo).sendInquireMessage();
            isReceivedReplyForInquire = false;
        } else if (lockedTo==c.getID()) { 
        	isReceivedaFailedMessage=true;
        	int tempID = lockedTo;
            long tempClock = lockedWithClock;
            if (popAndLock()==null)
            	return true;
            else
            	queue.add(new ClockIdPair(tempClock, tempID));
        }
    	return false;
    }
    
    private ClockIdPair popAndLock() {
    	ClockIdPair newLocker = queue.poll();
        if (newLocker!=null) {
        	lockedWithClock = newLocker.getClockValue();
        	lockedTo = newLocker.getID();
        	getTreeQuorum().find(lockedTo).sendLockedMessage();
        }
        return newLocker;
    }

    public boolean isInCriticalSection() {
        return isInCriticalSection;
    }

    private void releaseQuorum() {
        //TODO fill out the release of all connections in previous quorum
        if (myQuorum != null) {
            for (Connection conn : myQuorum.values()) {
            	if (!conn.hasID(c.getID()))
            		conn.sendReleaseMessage();
            }
        }
    }

    public void close() {
        releaseQuorum();
        SingletonTreeQuorum.closeTreeQuorum();
        c.destroySocket(cyclical_receiver);
        c.destroySocket(cyclical_sender);
        c.destroySocket(deathSock);
        c.destroySocket(outbound_receiver);
    }

    protected static class SingletonTreeQuorum {

        private static TreeQuorum tQ = null;

        private SingletonTreeQuorum() {
        }

        public static TreeQuorum getTreeQuorum() {
            if (tQ == null) {
                tQ = new TreeQuorum();
            }
            return tQ;
        }

        public static void closeTreeQuorum() {
            if (tQ != null) {
                tQ.close();
            }
            //getTreeQuorum().close();
            tQ = null;
        }
    }
}
