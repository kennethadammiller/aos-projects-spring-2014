package AOSAssignments.Project2.DMEImplementation.CustomJMockMatchers;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Socket;

public class ListMatcher extends
	TypeSafeMatcher<List<Entry<Integer, ZMQ.Socket>>> {
	
	private List<Entry<Integer, ZMQ.Socket>> expectedQuorum;
	private String errMsg;
	
	public static Matcher<List<Entry<Integer, ZMQ.Socket>>> quorumsMatch(
			List<Entry<Integer, ZMQ.Socket>> expected) {
		return new ListMatcher(expected);
	}

	private ListMatcher(List<Entry<Integer, ZMQ.Socket>> expQ) {
		expectedQuorum = expQ;
	}
	
	public void describeTo(Description arg0) {
		// TODO Auto-generated method stub
		arg0.appendText(errMsg);
	}

	@Override
	public boolean matchesSafely(List<Entry<Integer, Socket>> actualQuorum) {
		HashMap<Integer, Socket> map = new HashMap<Integer, Socket>();
		for (Entry<Integer, Socket> e : actualQuorum)
			map.put(e.getKey(), e.getValue());
		boolean equal = false;
		if (actualQuorum.size() != expectedQuorum.size()) {
			equal = false;
			errMsg +="actual Quorum and expected do not match size. ";
		}
		for (Entry<Integer, ZMQ.Socket> x : expectedQuorum) {
			if (!map.containsKey(x.getKey())) {
				errMsg+= "actualQuorum doesn't contain " + x.getKey() + " ";
				equal = false;
			}
		}
		return equal;
	}
}
