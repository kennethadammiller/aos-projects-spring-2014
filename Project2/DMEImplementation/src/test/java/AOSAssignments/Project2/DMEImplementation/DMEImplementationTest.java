package AOSAssignments.Project2.DMEImplementation;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DMEImplementationTest extends TQBase {
	
	@Before
	public void setUp() {
		super.setUp();
	}
	
	@After
	public void tearDown() {
		ctxt.term();
	}
	
	public void initSortedDataStructureTest(ArrayList<Integer> testInput) {
		
		int randSize = 50; //(new Random()).nextInt() % 50;
		for (int i=0;i<randSize;i++) {
			testInput.add(i);
		}
		Collections.shuffle(testInput);
	}
	
	@Test
	public void testSortedPriorityQueue() {
		ArrayList<Integer> testInput = new ArrayList<Integer>();
		initSortedDataStructureTest(testInput);
		PriorityQueue<Integer> shouldBeSorted = new PriorityQueue<Integer>();
		shouldBeSorted.addAll(testInput);
		int prev = shouldBeSorted.remove() ;
		for (int i=1;i<shouldBeSorted.size();i++) {
			int val = shouldBeSorted.remove();
			assertTrue("SortedArrayList not sorted; prev=" + prev + ", next="
					+ val, prev+1 == val);
			prev = val;
		}
	}
	
	@Test
	public void testCSEnter() {
		context.assertIsSatisfied();
	}
	
	@Test
	public void testCSLeave() {
		context.assertIsSatisfied();
	}
}
