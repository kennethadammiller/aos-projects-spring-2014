package AOSAssignments.Project2.DMEImplementation;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import org.jmock.Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.zeromq.ZMQ;

public class TQBase {
	protected static Mockery context = null;
	protected static ZMQ.Context ctxt=null;
	protected static int SOCKET_TYPE = ZMQ.PUB;
	protected static int NETWORK_SIZE=50;
	
	public void setUp() {
		context = new Mockery() {{
	        setImposteriser(ClassImposteriser.INSTANCE);
	    }};
		ctxt = ZMQ.context(0);
	}
	
	public static List<Entry<Integer, Connection>> constructNeighbors() {
		//TODO use config instead
		final List<Entry<Integer, Connection>> neighbors = 
				new LinkedList<Entry<Integer, Connection>>();
		for (int i=1; i<=NETWORK_SIZE; i++) {
			neighbors.add(
					new AbstractMap.SimpleEntry<Integer, Connection>(
							i, null)); //ctxt.socket(SOCKET_TYPE)));
		}
		return neighbors;
	}
	
	protected void terminateAllSockets(List<Entry<Integer, Connection>> socks) {
		if (socks==null)
			return;
		for (Entry<Integer, Connection> e : socks)
			if (e.getValue()==null) continue; 
			else e.getValue().close();
	}
	
	protected void terminateAllSockets(HashMap<Integer, Connection> map) {
		if (map==null)
			return;
		for (Connection c : map.values())
			if (c==null) continue;
			else c.close();
	}
}
