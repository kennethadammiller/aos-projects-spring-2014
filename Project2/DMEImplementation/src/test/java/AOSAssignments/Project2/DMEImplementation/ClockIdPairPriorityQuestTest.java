package AOSAssignments.Project2.DMEImplementation;

import static org.junit.Assert.*;

import java.util.PriorityQueue;
import java.util.Random;

import org.junit.Test;

public class ClockIdPairPriorityQuestTest {

	@Test
	public void testOrdering() {
		Random x = new Random();
		PriorityQueue<ClockIdPair> toTest = new PriorityQueue<ClockIdPair>();
		for (int i =0; i<50;i++) {
			for (int j=0;j<10;j++) {
				toTest.add(new ClockIdPair(Math.abs(x.nextInt()), 0));
			}
			int temp = -1;
			while (!toTest.isEmpty()) {
				assertTrue("PriorityQueue out of order!", temp < toTest.remove().getClockValue());
			}
			
		}
	}

}
