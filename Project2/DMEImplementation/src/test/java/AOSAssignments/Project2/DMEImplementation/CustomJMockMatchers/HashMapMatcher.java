package AOSAssignments.Project2.DMEImplementation.CustomJMockMatchers;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.zeromq.ZMQ;

import AOSAssignments.Project2.DMEImplementation.Connection;

public class HashMapMatcher extends
		TypeSafeMatcher<HashMap<Integer, Connection>> {
	HashMap<Integer, Connection> expectedQuorum;
	private String errMsg="";
	
	public static Matcher<HashMap<Integer, Connection>> quorumsMatch(
			HashMap<Integer, Connection> expected) {
		return new HashMapMatcher(expected);
	}

	private HashMapMatcher(HashMap<Integer, Connection> expQ) {
		expectedQuorum = expQ;
	}
	
	private HashMapMatcher(List<Entry<Integer, Connection>> expQ) {
		expectedQuorum = new HashMap<Integer, Connection>();
		for (Entry<Integer, Connection> e : expQ)
			expectedQuorum.put(e.getKey(), e.getValue());
	}

	public boolean matchesSafely(HashMap<Integer, Connection> actualQuorum) {
		boolean equal = true;
		if (actualQuorum.size() != expectedQuorum.size()) {
			equal = false;
			errMsg +="actual Quorum and expected do not match size. ";
		}
		for (Entry<Integer, Connection> x : expectedQuorum.entrySet()) {
			if (!actualQuorum.containsKey(x.getKey())) {
				errMsg+= "actualQuorum doesn't contain " + x.getKey() + " ";
				equal = false;
			}
		}
		return equal;
	}

	public void describeTo(Description arg0) {
		// TODO Auto-generated method stub
		arg0.appendText(errMsg);
	}
}
