package AOSAssignments.Project2.DMEImplementation;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.jmock.Expectations;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.zeromq.ZMQ;

import AOSAssignments.Project2.DMEImplementation.TreeQuorum.AvlNode;
import AOSAssignments.Project2.DMEImplementation.CustomJMockMatchers.HashMapMatcher;
import AOSAssignments.Project2.DMEImplementation.CustomJMockMatchers.ListMatcher;
import AOSAssignments.Project2.NetworkConfiguration.Config;

public class TreeQuorumTest extends TQBase {
	private List<Entry<Integer, Connection>> neighbors;
	private Set<Integer> actualQuorum;

	@Before
	public void setUp() {
		super.setUp();
	}

	@After
	public void tearDown() {
		terminateAllSockets(neighbors);
		// terminateAllSockets(actualQuorum);
		ctxt.term();
	}

	private void assertFundamentalTreeProperty(AvlNode n) {
		String errPrefix = "fundamental tree property violated";
		if (n.left != null) {
			assertTrue(errPrefix + " left child > parent", n.left.key < n.key);
			assertFundamentalTreeProperty(n.left);
		}
		if (n.right != null) {
			assertTrue(errPrefix + " right child < parent ",
					n.right.key > n.key);
			assertFundamentalTreeProperty(n.right);
		}
	}

	@Test
	public void testAVLTreeOrdering() {
		int temp = NETWORK_SIZE;
		NETWORK_SIZE = 7;

		neighbors = constructNeighbors();
		// Collections.shuffle(neighbors);

		TreeQuorum tQuorum = new TreeQuorum(neighbors);
		tQuorum.tree.recursiveBalance(tQuorum.tree.root);
		ArrayList<AvlNode> nodes = tQuorum.tree.inorder();

		int i = 1;
		for (AvlNode n : nodes) {
			assertTrue("keys unequal: n.key=" + n.key + ", exp=" + i + "; "
					+ tQuorum.tree.inorder(), n.key == i++);
		}
		assertFundamentalTreeProperty(tQuorum.tree.root);
		nodes = tQuorum.tree.preorder();
		tQuorum.tree.recursiveBalance(tQuorum.tree.root);
		int arr[] = { 4, 2, 1, 3, 6, 5, 7 };
		i = 0;
		for (AvlNode n : nodes) {
			assertTrue("keys unequal: n.key=" + n.key + ", exp=" + arr[i]
					+ "; " + nodes, n.key == arr[i++]);
		}
		nodes = tQuorum.tree.postorder();
		tQuorum.tree.recursiveBalance(tQuorum.tree.root);
		int[] arr2 = { 1, 3, 2, 5, 7, 6, 4 };
		i = 0;
		for (AvlNode n : nodes) {
			assertTrue("keys unequal: n.key=" + n.key + ", exp=" + arr2[i] + i
					+ "; " + nodes, n.key == arr2[i++]);
		}
		NETWORK_SIZE = temp;
	}

	@Test
	public void testGetQuorumSuccess() {
		int temp = NETWORK_SIZE;
		NETWORK_SIZE = 7;
		final Connection conn = context.mock(Connection.class);
		// Maps tested key to quorum returned.
		Config.getConfig().setID(1);
		neighbors = constructNeighbors();
		HashMap<Integer, Connection> mocked_neighbors = new HashMap<Integer, Connection>();
		for (Entry<Integer, Connection> e : neighbors) {
			mocked_neighbors.put(e.getKey(), conn);
		}
		TreeQuorum tQ = new TreeQuorum(mocked_neighbors);
		final HashMap<Integer, Connection> expectedQuorum = 
				new HashMap<Integer, Connection>();

		runQuorumTest(1, new int[]{4,2,1}, expectedQuorum, tQ, conn);
		//////////////
		runQuorumTest(3, new int[]{4, 2, 3}, expectedQuorum, tQ, conn);
		////////////////
		runQuorumTest(5, new int[]{4,6,5}, expectedQuorum, tQ, conn);
		/////////////////
		runQuorumTest(7, new int[]{4,6,7}, expectedQuorum, tQ, conn);
		/////////////////
		runQuorumTest(6, new int[]{4,6,7,5}, expectedQuorum, tQ, conn);
	}
	
	private void runQuorumTest(int ID, int[] vals,
			HashMap<Integer, Connection> expectedQuorum, 
			TreeQuorum tQ, final Connection conn) {
		HashMap<Integer, Connection> result = setUpQuorumTest(ID, vals, expectedQuorum, tQ,
				conn);
		checkResult(result, expectedQuorum);
	}
	
	private HashMap<Integer, Connection> setUpQuorumTest(int ID, int[] vals,
			HashMap<Integer, Connection> expectedQuorum, TreeQuorum tQ, 
			final Connection conn) {
		Config.getConfig().setID(ID);
		expectedQuorum.clear();
		for (int i=0; i<vals.length;i++) {
			expectedQuorum.put(vals[i], null);
		}
		context.checking(new Expectations() {
			{
				atLeast(3).of(conn).grantsPermission();
				will(returnValue(true));
			}
		});
		HashMap<Integer, Connection> actualResult = tQ.getQuorum();

		return actualResult;
	}
	
	private void checkResult(HashMap<Integer, Connection> result, 
			HashMap<Integer, Connection> expected) {
		assertTrue("getQuorum returns null", result != null);
		actualQuorum = result.keySet();
		assertTrue("actual quorum null", actualQuorum != null);
		assertTrue("actual quorum empty", actualQuorum.size()!=0);
		if (expected.keySet().size()==result.keySet().size()) {
			assertTrue("unexpected quorum calculated: " + actualQuorum,
				expected.keySet().equals(actualQuorum));
		} else if (expected.keySet().size() >= result.keySet().size()) {
			for (Iterator<Integer> i=result.keySet().iterator(); 
					i.hasNext();) {
				assertTrue("unexpected quorum calculated: " + actualQuorum + 
						", expected: " + expected.keySet(), 
						expected.containsKey(i.next()));
			}
		}
	}
	
	//@Test
	public void testGetQuorumFailureResilient() {
		int temp = NETWORK_SIZE;
		NETWORK_SIZE = 7;
		final Connection conn = context.mock(Connection.class);
		// /////////////////////////////
		// For requesting leaf node 1 of network size 7, failure of node 2...
		HashMap<Integer, Connection> network = new HashMap<Integer, Connection>();
		HashMap<Integer, Connection> expected = null;
		for (int i = 0; i < NETWORK_SIZE; i++)
			network.put(i, conn);

		TreeQuorum tQ = new TreeQuorum(network);
		expected = new HashMap<Integer, Connection>();
		
		// /////////////////////////////
		// For requesting leaf node 3 of network size 7, failure of node 2...
		runQuorumFailureResilienceTest(3, new int[] {4, 1, 3}, expected, tQ, conn);
		// /////////////////////////////
		// For requesting leaf node 5 of network size 7, failure of node 6...
		runQuorumFailureResilienceTest(5, new int[] {4, 5, 7}, expected, tQ, conn);
		// /////////////////////////////
		// For requesting leaf node 4 of network size 7, failure of node 4...
		NETWORK_SIZE = temp;
	}
	
	private void runQuorumFailureResilienceTest(int ID, int[] vals,
			HashMap<Integer, Connection> expectedQuorum, TreeQuorum tQ, 
			final Connection conn) {
		HashMap<Integer, Connection> result = setUpQuorumFailureTest(
				ID, vals, expectedQuorum, tQ, conn);
		checkResult(result, expectedQuorum);
	}
	
	private HashMap<Integer, Connection> setUpQuorumFailureTest(int ID, int []vals,
			HashMap<Integer, Connection> expectedQuorum, TreeQuorum tQ, final Connection conn) {
		Config.getConfig().setID(ID);
		expectedQuorum.clear();
		for (int i=0; i<vals.length;i++) {
			expectedQuorum.put(vals[i], null);
		}
		context.checking(new Expectations() {
			{
				oneOf(conn).grantsPermission(); // root node; 4
				will(returnValue(true));
				oneOf(conn).grantsPermission(); // node 2, failure
				will(returnValue(false));
				oneOf(conn).grantsPermission(); // node 1
				will(returnValue(true));
				oneOf(conn).grantsPermission(); // node 3
				will(returnValue(true));
			}
		});
		return tQ.getQuorum();
	}

	@Test
	public void testGetQuorumAbsoluteFailure() {
		context.assertIsSatisfied();
	}
}
