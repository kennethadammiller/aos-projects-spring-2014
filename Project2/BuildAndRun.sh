cd ~/
rm -rf ~/apache-maven*
wget "http://www.eng.lsu.edu/mirrors/apache/maven/maven-3/3.2.1/binaries/apache-maven-3.2.1-bin.zip"
unzip apache-maven-3.2.1-bin.zip -d apache-maven
cd ~/Project2/
alias mvn="~/apache-maven/apache-maven-3.2.1/bin/mvn"
mvn clean install
if [[ -z $user_name ]] 
then
	echo "You have to export user_name='your_user_name'"
	exit -1
fi

ssh $user_name@net01.utdallas.edu 'bash -s' < RunDMEService.sh 1
ssh $user_name@net02.utdallas.edu 'bash -s' < RunDMEService.sh 2
ssh $user_name@net03.utdallas.edu 'bash -s' < RunDMEService.sh 3
ssh $user_name@net04.utdallas.edu 'bash -s' < RunDMEService.sh 4
ssh $user_name@net05.utdallas.edu 'bash -s' < RunDMEService.sh 5
ssh $user_name@net06.utdallas.edu 'bash -s' < RunDMEService.sh 6
ssh $user_name@net07.utdallas.edu 'bash -s' < RunDMEService.sh 7

sleep 20
cat CriticalSectionLog.txt

